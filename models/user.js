const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "first name is required"]
	},

	lastName : {
		type: String,
		required: [true, "last name is required"]
	},

	email : {
		type: String,
		required : [true, "email is required"]
	},

	mobileNo : {
		type: String,
		required : [true, "mobile number is required"]
	},

	password : {
		type: String, 
		default: true
	},

	isAdmin: {
		type: Boolean, 
		default: false
	},

	enrollments : [

		{
			courseId: {
				type : String,
				required: [true, "UserId of enrollee is required"]
			},

			enrolledOn:{
				type: Date,
				default: new Date()
			},

			status:{
				type: String,
				default: "enrolled"
			}	
		}

	]
})

module.exports = mongoose.model('User', userSchema);